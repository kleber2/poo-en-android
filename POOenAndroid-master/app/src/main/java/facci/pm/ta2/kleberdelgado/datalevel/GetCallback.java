package facci.pm.ta2.kleberdelgado.datalevel;


public interface GetCallback<DataObject> {
    public void done(DataObject object, DataException e);
}
