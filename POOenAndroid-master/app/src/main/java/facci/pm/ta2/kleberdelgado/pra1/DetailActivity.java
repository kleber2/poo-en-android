package facci.pm.ta2.kleberdelgado.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.kleberdelgado.datalevel.DataException;
import facci.pm.ta2.kleberdelgado.datalevel.DataObject;
import facci.pm.ta2.kleberdelgado.datalevel.DataQuery;
import facci.pm.ta2.kleberdelgado.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity  {
    private TextView name, price, description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");

        // INICIO - CODE6
        //PREGUNTA 3.1
        //Acceso al object id recibido como parámetro
        String object_id = getIntent().getStringExtra("object_id");

        //PREGUNTA 3.2
        //El Uso del setMovementMethod para añadir scrollbars al campo descripcion
        TextView descripcion = (TextView) findViewById(R.id.description);
        descripcion.setMovementMethod(LinkMovementMethod.getInstance());

        //PREGUNTA 3.3
        //El Acceso a las propiedades del object de tipo String: name, price, description, e image;
        DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e == null){
                    TextView name = (TextView)findViewById(R.id.name);
                    TextView price = (TextView)findViewById(R.id.price);
                    TextView description = (TextView)findViewById(R.id.description);
                    ImageView thumbnail = (ImageView)findViewById(R.id.thumbnail);


                    //PREGUNTA 3.4
                    //rellenar las layout con las propiedades
                    name.setText((String) object.get("name"));
                    price.setText((String) object.get("price")+"\u0024");
                    description.setText((String) object.get("description"));
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));
                }
            }
        });
        // FIN - CODE6

    }




}